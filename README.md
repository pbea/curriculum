# *Currículum Prova*

![foto](https://assets.gitlab-static.net/uploads/-/system/user/avatar/2524348/avatar.png?width=23)

**Nom:** Pep Beà

**Adreça:** c/Diagonal num1

**Telèfon:** 666 666 666

**Mail:** pbea@iespoblenou.org

**Web:** [LaMevaWeb](http://www.google.com)


## Actualment

**Estudiantt:** cursant 2n SMIX

**Treballant:** Administrador de Sistemes a ProInf.com

## Estudis

**ESO:** A l'INS Escola Joan Fus amb nota de *notable*


## Idiomes


Idioma | comprensió oral | comprensió escrita | expressió oral | expressió escrita 
---|---|---|---|---
Català | molt bo | molt bo | molt bo | molt bo
Castellà | molt bo | molt bo | molt bo | molt bo
Francès |  bo |  bo |  bo |  bo
Alemany | bàsic | bàsic | bàsic | bàsic


## Hobbies

> M'agrada jugar a escacs, sóc un apassionat de la mountain bike i segueixo el futbol

## Capacitats transversals

- Treball en equip
- Autònom
- Sociable
- Treballador
- Responsable

## Futur i aspiracions

M'agradaria continuar els meus estudis:
- [x] Treure'm el 1r de SMIX.
- [ ] Realitzar el cicle formatiu de grau superior de DAM.
- [ ] Treure'm el carnet de conduir
- [ ] Obtenir un certificat d'anglès
    

## Codi que m'agrada

```java
public class HelloWorld {

    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");
    }

}
```

